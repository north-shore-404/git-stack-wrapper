# What is this?
Many development projects involve multiple repositories connected in various ways. This is a repository meant for wrapping those related repositories, presumably those within a group, so that an entire project can be pulled and setup with only a few actions.

*Note: this boilerplate is meant for simplifying the onboarding process and supplying convenient commands for running a stack. This is **not** a wrapper for git.*

### Requirements
This boilerplate uses bash so if you are on Windows you will need to run within WSL2 and it is *highly recommended* that you keep your project files within the Linux subsystem (eg. `/home/local/your-group`). If your projects use `node` you must have `nvm` installed for these scripts to work. If your projects use `composer` you will need to have it installed.

[Installation](#installation)

[Usage](#usage)

[Setup Boilerplate](#setup-boilerplate)

---

# Installation
Follow these directions to clone and prepare your group. Setup SSH access to this repository and each submodule for easiest installation.

## git clone
Grab the group repository.
```bash
git clone git@gitlab.com:your-group/your-group.git
```

## nvm exec
This will do a few things, first it installs `concurrently` for conveniently running your stack. Next, it runs `npm install` within each directory containing a `package.json` file. And finally it runs `composer install` within each directory containing a `composer.json` file.

```bash
nvm exec npm install
```

---

# Usage
This repository is **not** meant to manage git for submodules. All version control should be handled within the individual repository.

Using `concurrently` to run your stack:

```bash
nvm exec npm start
```

### start-stack.bat
If you use Windows 10 with WSL2 you can create a convenient shortcut for starting your stack. Simply copy the provided `start-stack.bat` to somewhere convenient within Windows and it will run `nvm exec npm start` within this directory when opened.

**Note: copy file only after Installation, the path within the batch file is automatically updated during this process.**

---

# Setup boilerplate
Follow these directions to integrate your repositories into this wrapper.

## git clone
Clone this repository and prepare for installation.

```bash
# change `group-name` to your group slug
git clone https://gitlab.com/north-shore-404/git-stack-wrapper group-name
cd group-name

# remove boilerplate git, setup your own remote
rm -rf ./.git
git init
git remote add origin git@gitlab.com:your-group/your-group.git

# update .nvmrc if using node
echo -e "16.13.2" > .nvmrc
```

### one-liner of above commands, set 'GROUP_NAME' to your group
```bash
(export GROUP_NAME=group-name && mkdir $GROUP_NAME && cd $GROUP_NAME && git clone https://gitlab.com/north-shore-404/git-stack-wrapper . && rm -rf ./.git && git init && git remote add origin git@gitlab.com:$GROUP_NAME/$GROUP_NAME.git && echo -e "16.13.2" > .nvmrc)
```

## git submodule
Add each repository within the group, I recommend using SSH addressses.

```bash
# add each repository as a submodule
git submodule add git@gitlab.com:your-group/repository1.git
git submodule add git@gitlab.com:your-group/repository2.git
...
```

## npm scripts
If your project has multiple webservers and/or watchers you can now setup project specific scripts for  `concurrently` to use. By default the `npm start` command will concurrently run any scripts matching `start-*`. A few example scripts are included and prefixed with and underscore - feel free to update these to fit your project and then remove the underscore to automatically include it in the `start` command. See [concurrently README](https://github.com/open-cli-tools/concurrently) for more info.

## git commit
Commit and push!
```bash
# commit and push
git add -A
git commit -m "Initial commit"
git push origin master
```

## install
Setup is now complete, continue to the [Installation section](#installation) section, obviously you can skip the `clone` step.